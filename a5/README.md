> **NOTE:** This README.md file should be placed at the **root of each of your repos directories.**
>
>Also, this file **must** use Markdown syntax, and provide project documentation as per below--otherwise, points **will** be deducted.
>

# LIS4381 - Mobile/Web Application Development and Management

## Devonte Aird

### Assignment 5 Requirements:

*Seven Parts:*

1. Modify index and header.php
2. Move a4 index and rename to "add_petstore.php" 
3. Modify provided php files 
4. Add server-side validation in "add_petstore_process.php"
5. Provide screenshots of failed validation and a5 index.php
6. Provide screenshots of skillset php files
7. Provide screenshots of skillset Java program

#### README.md file should include the following items:

* Coursetitle, your name, assignment requirements, as per A1
* Screenshots as per below examples
* Link to local lis4381 web app: http://localhost/repos/lis4381

#### Assignment Screenshots:

*Link to local LIS4381 main page [My Online Portfolio](http://localhost/repos/lis4381 "PHP Localhost")*:

| A5 Main Page | Failed Validation |
| --- | --- |
| ![A5 Main Page](img/index.png) | ![Failed Validation](img/error.png) |

*Screenshot of Java Skillset 13:*

![Skillset 13 Screenshot](img/ss13.png)

*GIF of PHP Skillset 14:*

![Skillset 14 GIF](img/simple_calc.gif)

*Screenshots of PHP Skillset 15:*

| Write | Read | 
| --- | --- |
| ![Write Page](img/write.png) | ![Read Page](img/read.png) | 