> **NOTE:** This README.md file should be placed at the **root of each of your repos directories.**
>
>Also, this file **must** use Markdown syntax, and provide project documentation as per below--otherwise, points **will** be deducted.
>

# LIS4381 - Mobile/Web Application Development and Management

## Devonte Aird

### Assignment 3 Requirements:

*Four Parts:*

1. Create an ERD with 10 data inserts in each table and forward engineer to local
2. Java skillsets 4-6
3. Chapter Questions (Chs 5, 6)
4. Create a ticket price app using Android Studio

#### README.md file should include the following items:

* Coursetitle, your name, assignment requirements, as per A1
* Screenshot of ERD
* Screenshot of running application’s first user interface
* Screenshot ofrunning application’s second user interface 
*  Links to the following files:
    1. a3.mwb
    2. a3.sql

#### Assignment Screenshots:

*Screenshot of ERD*:

![ERD Screenshot](img/erd.png)


*Screenshots of Running app*

| Screen 1 | Screen 2 |
| --- | --- |
| ![Screen 1](img/screen1_300x700.png) | ![Screen 2](img/screen2_300x700.png) |


*Screenshots of Java Skillsets*

| Skillset 4 | Skillset 5 | Skillset 6 |
| --- | --- | --- | 
| ![Skillset 4](img/ss4.png) | ![Skillset 5](img/ss5.png) | ![Skillset 6](img/ss6.png) |


*Links for mwb and sql files*:

[Link for mwb file](A3.mwb)

[Link for sql file](a3.sql)