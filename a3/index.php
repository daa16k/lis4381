<!DOCTYPE html>
<html lang="en">
  <head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
		<meta name="description" content="My online portfolio that illustrates skills acquired while working through various project requirements.">
		<meta name="author" content="Devonte Aird">
    <link rel="icon" href="favicon.ico">

		<title>LIS4381 - Assignment3</title>		
		<?php include_once("../css/include_css.php"); ?>			
  </head>
	
	<style>
table, th, td {
  border: 1px solid black;
}
table.center {
  margin-left: auto;
  margin-right: auto;
}
</style>

  <body>

		<?php include_once("../global/nav.php"); ?>
		
		<div class="container">
			<div class="starter-template">
				<div class="page-header">
					<?php include_once("global/header.php"); ?>	
				</div>
				<p class="text-justify">
					<strong>Description:</strong> The expected norm...
Create an ERD with 10 data inserts in each table and forward engineer to local,
Java skillsets 4-6,
Chapter Questions (Chs 5, 6),
Create a ticket price app using Android Studio.


				<h4>ERD Screenshot</h4>
				<img src="img/erd.png" class="img-responsive center-block" alt="ERD screenshot">

				<h4>Android Screenshots</h4>
				
				<table class="center">

				<tr>

				<th>
				<img src="img/screen1_300x700.png"/>
				</th>

				<th>
				<img src="img/screen2_300x700.png"/>
				</th>

				</tr>

				</table>

				<h4>Skillset Screenshots</h4>
				
				<table class="center" style="width:400px">

				<tr>

				<td>
				<img src="img/ss4600x600.png"/>
				</td>

				<td>
				<img src="img/ss5_600x600.png"/>
				</td>
		
				</tr>

				</table>
				
				<img src="img/ss6.png" class="img-responsive center-block">
				
				<h4>Relative URLs</h4>
<p><a href="A3.mwb">A3 mwb file</a></p>
<p><a href="A3.sql">A3 SQL file</a></p>

				
				<?php include_once "global/footer.php"; ?>

			</div> <!-- starter-template -->
    </div> <!-- end container -->

		<!-- Bootstrap JavaScript
				 ================================================== -->
		<!-- Placed at end of document so pages load faster -->		
		<?php include_once("../js/include_js.php"); ?>			
  </body>
</html>
