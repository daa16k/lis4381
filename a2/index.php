<!DOCTYPE html>
<html lang="en">
  <head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
		<meta name="description" content="My online portfolio that illustrates skills acquired while working through various project requirements.">
		<meta name="author" content="Devonte Aird">
    <link rel="icon" href="favicon.ico">

		<title>LIS4381 - Assignment2</title>		
		<?php include_once("../css/include_css.php"); ?>			
  </head>
	
	<style>
table, th, td {
  border: 1px solid black;
}
table.center {
  margin-left: auto;
  margin-right: auto;
}
</style>

  <body>

		<?php include_once("../global/nav.php"); ?>
		
		<div class="container">
			<div class="starter-template">
				<div class="page-header">
					<?php include_once("global/header.php"); ?>	
				</div>
				<p class="text-justify">
					<strong>Description:</strong> The expected norm...
Create a mobile recipe app with Android Studio,
Provide screenshots for user interfaces,
Chapter Questions (Chs 3, 4),
Screenshots for the completed skillsets

				<h4>Android Screenshots</h4>
				
				<table class="center">

				<tr>

				<th>
				<img src="img/android_2_small.png"/>
				</th>

				<th>
				<img src="img/android_1_small.png"/>
				</th>

				</tr>

				</table>

				<h4>Skillset Screenshots</h4>
				
				<table class="center" style="width:400px">

				<tr>

				<td>
				<img src="img/skillset1_small.png"/>
				</td>

				<td>
				<img src="img/skillset2_small.png"/>
				</td>
		
				</tr>

				</table>
				
				<img src="img/skillset3_small.png" class="img-responsive center-block">

				
				<?php include_once "global/footer.php"; ?>

			</div> <!-- starter-template -->
    </div> <!-- end container -->

		<!-- Bootstrap JavaScript
				 ================================================== -->
		<!-- Placed at end of document so pages load faster -->		
		<?php include_once("../js/include_js.php"); ?>			
  </body>
</html>
