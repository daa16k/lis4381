> **NOTE:** This README.md file should be placed at the **root of each of your repos directories.**
>
>Also, this file **must** use Markdown syntax, and provide project documentation as per below--otherwise, points **will** be deducted.
>

# LIS4381 - Mobile/Web Application Development and Management

## Devonte Aird

### Assignment 2 Requirements:

*Four Parts:*

1. Create a mobile recipe app with Android Studio
2. Provide screenshots for user interfaces
3. Chapter Questions (Chs 3, 4)
4. Screenshots for the completed skillsets

#### README.md file should include the following items:

* Screenshots of running Java skillsets;
* Screenshots of running Android Studio - "Bruschetta Recipe"

#### Assignment Screenshots:
| Screen 1 | Screen 2 |
| --- | --- |
| ![Screen 1](img/android_2_small.png) | ![Screen 2](img/android_1_small.png) |

[//]: # (skillset screenshots)

| Skillset 1 | Skillset 2 | Skillset 3 |
| --- | --- | --- | 
| ![Skillset 1](img/skillset1.png) | ![Skillset 2](img/skillset2.png) | ![Skillset 3](img/skillset3.png) |
