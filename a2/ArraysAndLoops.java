class ArraysAndLoops
{
    public static void main(String args[])
    {
        System.out.println("Developer: Devonte Aird");
        System.out.println("Program loops through array of strings.");
        System.out.println("Use following values: dog, cat, bird, fish, insect.");
        System.out.println("Use following structures: for, enhanced for, while, do...while.\n");
        System.out.println("Note: Pretest loops: for, enhanced for, while. Posttest loop: do...while.\n");

        //create array
        String[] animals = {"dog", "cat", "bird", "fish", "insect"};

        //for loop to print the array
        System.out.println("for loop");
        for (int i=0; i < animals.length; i++){
            System.out.println(animals[i]);
        }

        //blank line
        System.out.println(); 

        //enchanced for loop
        System.out.println("Enhanced for loop:");
        for (String x : animals) {
            System.out.println(x);
        }

        //blank line
        System.out.println(); 

        //while loop
        System.out.println("While loop:");
        int y = 0;
        while (y < animals.length) {
            System.out.println(animals[y]);
            y++;
        }

        //insert blank line
        System.out.println();

        //do...while loop to print the array
        System.out.println("do...while loop"); 
        int z = 0;
        do {
            System.out.println(animals[z]); 
            z++;
        } while (z < animals.length);
        System.out.println(); 
        
    }// end of main
}// end of class