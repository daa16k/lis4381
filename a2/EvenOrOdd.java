import java.util.Scanner;
public class EvenOrOdd
{
    public static void main(String[] args)
    {
        //prompt user for input
        System.out.println("Developer: Devonte Aird");
        System.out.println("Program evaluates integers as even or odd");
        System.out.println("Note: Program does *not* check for non-numeric characters. \n");


        // declare and initialize variables, create Scanner object, recieve user input
        int x = 0;
        System.out.print("Enter integer: ");
        Scanner input= new Scanner(System.in);
        x = input.nextInt();

        if (x % 2 == 0)
        {
        System.out.println(x + " is an even number.");   
        }
        else
        {
        System.out.println(x + " is an odd number.");  
        }

    }// end of main

}// end of class