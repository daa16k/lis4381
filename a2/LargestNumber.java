import java.util.Scanner; 

public class LargestNumber {
    public static void main(String[] args) {
        //print program details
        System.out.println("Developer: Devonte Aird");
        System.out.println("Program evaluates largest of two integers.");
        System.out.println("Note: Program does *not* check for non-numeric characters or non-integer values.\n");

        //declare variables
        int num1;
        int num2;

        //create Scanner object
        Scanner input= new Scanner(System.in);

        //prompt and recieve input
        System.out.print("Enter first integer: ");
        num1 = input.nextInt();

        System.out.print("Enter second integer: ");
        num2 = input.nextInt();

        System.out.println(); //line for spacing

        //evaluate user input
        if (num1 == num2) {
            System.out.println("Integers are equal.");
        }
        else if (num1 > num2) {
            System.out.printf("%d is larger than %d%n", num1, num2);
        }
        else {
            System.out.printf("%d is larger than %d%n", num2, num1);
        }
    }// end of main

}// end of class