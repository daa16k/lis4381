> **NOTE:** This README.md file should be placed at the **root of each of your repos directories.**
>
>Also, this file **must** use Markdown syntax, and provide project documentation as per below--otherwise, points **will** be deducted.
>

# LIS4381 - Mobile/Web Application Development and Management

## Devonte Aird

### Assignment 1 Requirements:

*Four Parts:*

1. Distributed Version Control with Git and Bitbucket
2. Development Installations
3. Chapter Questions (Chs 1, 2)
4. Bitbucket repo links: a) this assignment and b) the completed tutorials above (bitbucketstationlocations)

#### README.md file should include the following items:

* Screenshot of AMPPS Installation [My PHP Installation](http://localhost:8080 "PHP Localhost");
* Screenshot of running Java Hello;
* Screenshot of running Android Studio - My First App
* git commands w/short descriptions;

> This is a blockquote.
> 
> This is the second paragraph in the blockquote.
>
> #### Git commands w/short descriptions:

1. git init - Creates a git repo
2. git status - Checks the current state of the repo
3. git add - Marks changes to be included in the next commit
4. git commit - Save changes to the local repo
5. git push - Push new local commits on to a remote server
6. git pull - Fetch and download content from a remote repository and immediately update the local repository to match that content
7. One additional git command (git fetch) - Downloads new data from a remote repository - but it doesn't integrate any of this new data into the working files

#### Assignment Screenshots:

*Screenshot of AMPPS running [My PHP Installation](http://localhost:8080 "PHP Localhost")*:

![AMPPS Installation Screenshot](img/ampps.png)

*Screenshot of running java Hello*:

![JDK Installation Screenshot](img/jdk_install.png)

*Screenshot of Android Studio - My First App*:

![Android Studio Installation Screenshot](img/android.png)


#### Tutorial Links:

*Bitbucket Tutorial - Station Locations:*
[A1 Bitbucket Station Locations Tutorial Link](https://bitbucket.org/username/bitbucketstationlocations/ "Bitbucket Station Locations")

*Tutorial: Request to update a teammate's repository:*
[A1 My Team Quotes Tutorial Link](https://bitbucket.org/username/myteamquotes/ "My Team Quotes Tutorial")
