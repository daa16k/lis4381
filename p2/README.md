> **NOTE:** This README.md file should be placed at the **root of each of your repos directories.**
>
>Also, this file **must** use Markdown syntax, and provide project documentation as per below--otherwise, points **will** be deducted.
>

# LIS4381 - Mobile/Web Application Development and Management

## Devonte Aird

### Project 2 Requirements:

*Four Parts:*

    1. Suitably modify meta tags
    2. Change title, navigation links, and header tags appropriately
    3. Turn off client-side validation  
    4. Add server-side validation and regular expressions--as per the database entity attribute requirements
    5. Provide screenshots of php pages and RSS feed

#### README.md file should include the following items:

* Coursetitle, your name, assignment requirements, as per A1
* Screenshots as per below examples
* Link to local lis4381 web app: http://localhost/repos/lis4381

#### Assignment Screenshots:

*Link to local LIS4381 main page [My Online Portfolio](http://localhost/repos/lis4381 "PHP Localhost")*:

*P2 Main Page*:
![Data Entry Page](img/p2_index.png)

*P1 Data Entry Page*:
![Data Entry Page](img/index.png)

*Failed Validation*:
![Failed Validation](img/error.png) 

*Screenshot of RSS Feed*:

![RSS Screenshot](img/rss_ss.png)

*Carousel (Home Page)*:

![Carousel Screenshot](img/home.png)