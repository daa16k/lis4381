> **NOTE:** This README.md file should be placed at the **root of each of your repos directories.**
>
>Also, this file **must** use Markdown syntax, and provide project documentation as per below--otherwise, points **will** be deducted.
>

# LIS4381 - Mobile/Web Application Development and Management

## Devonte Aird

### Assignment 4 Requirements:

*Seven Parts:*

1. Move repos directory
2. Modify provided php file and subsequent files
3. Create favicon
4. Add jQuery validation and regular expressions for "petstore" entity
5. Provide screenshots of successful and failed validation
6. Provide screenshot of main page
7. Provide screenshots of skillset Java programs

#### README.md file should include the following items:

* Coursetitle, your name, assignment requirements, as per A1
* Screenshots as per below examples
* Link to local lis4381 web app: http://localhost/repos/lis4381

#### Assignment Screenshots:

*Screenshot of LIS4381 main page [My Online Portfolio](http://localhost/repos/lis4381 "PHP Localhost")*:

![Main Page Screenshot](img/carousel.gif)

| Screen 1 | Screen 2 |
| --- | --- |
| ![Screen 1](img/screen1.png) | ![Screen 2](img/screen2.png) |

[//]: # (skillset screenshots)

| Skillset 10 | Skillset 11 | Skillset 12 |
| --- | --- | --- | 
| ![Skillset 10](img/ss10.png) | ![Skillset 11](img/ss11.png) | ![Skillset 12](img/ss12.png) |