> **NOTE:** This README.md file should be placed at the **root of each of your repos directories.**
>
>Also, this file **must** use Markdown syntax, and provide project documentation as per below--otherwise, points **will** be deducted.
>

# LIS4381 - Mobile/Web Application Development and Management

## Devonte Aird

### LIS4381 Requirements:

*Course Work Links:*

1. [A1 README.md](a1/README.md "My A1 README.md file")
    * Install Git
    * Create Bitbucket repo
    * Complete Bitbucket tutorial (bitbucketstationlocations)
    * Install AMMPS
    * Install Java
    * Install Android Studio
    * Provide Git command descriptions

2. [A2 README.md](a2/README.md "My A2 README.md file")
    * Create Recipe app on Android Studio
    * Provide screenshots of completed app

3. [A3 README.md](a3/README.md "My A3 README.md file")
    * Create a database and ERD based upon the provided business rules
    * Provide screenshot of completed ERD
    * Provide screenshots of skillset Java programs
    * Create a mobile application that calculates ticket prices using Android Studio
    * Provide screenshots of working app

4. [A4 README.md](a4/README.md "My A4 README.md file")
    * Move repos directory
    * Modify provided php file and subsequent files
    * Create favicon
    * Add jQuery validation and regular expressions for "petstore" entity
    * Provide screenshots of successful and failed validation
    * Provide screenshot of main page
    * Provide screenshots of skillset Java programs

5. [A5 README.md](a5/README.md "My A5 README.md file")
    * Modify index and header.php
    * Move a4 index and rename to "add_petstore.php" 
    * Modify provided php files 
    * Add server-side validation in "add_petstore_process.php"
    * Provide screenshots of failed validation and a5 index.php
    * Provide screenshots of skillset php files
    * Provide screenshots of skillset Java program

*Projects:*

1. [P1 README.md](p1/README.md "My P1 README.md file")
    * Create a mobile business card app with Android Studio
    * Provide screenshots for live user interfaces
    * Provide screenshots of skillset Java programs

2. [P2 README.md](p2/README.md "My P2 README.md file")
    * Open index.php and review code
    * Suitably modify meta tags
    * Change title, navigation links, and header tags appropriately
    * Turn off client-side validation  
    * Add server-side validation and regular expressions--as per the database entity attribute requirements
    * Create RSS feed