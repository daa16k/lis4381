> **NOTE:** This README.md file should be placed at the **root of each of your repos directories.**
>
>Also, this file **must** use Markdown syntax, and provide project documentation as per below--otherwise, points **will** be deducted.
>

# LIS4381 - Mobile/Web Application Development and Management

## Devonte Aird

### Project 1 Requirements:

*Seven Parts:*

1. Create a launcher icon image and display it in both activities (screens)
2. Must add background color(s) to both activities
3. Must add border around image and button
4. Must add text shadow(button)
5. Provide screenshots of working app
6. Screenshots for the completed skillsets
7. Chapter Questions (Chs 7, 8)

#### README.md file should include the following items:

* Screenshots of running Java skillsets;
* Screenshots of running Android Studio - "My Business Card!"

#### Assignment Screenshots:
| Screen 1 | Screen 2 |
| --- | --- |
| ![Screen 1](img/screen1_300x700.png) | ![Screen 2](img/screen2_300x700.png) |

[//]: # (skillset screenshots)

| Skillset 7 | Skillset 8 | Skillset 9 |
| --- | --- | --- | 
| ![Skillset 7](img/ss7.png) | ![Skillset 8](img/ss8.png) | ![Skillset 9](img/ss9.png) |